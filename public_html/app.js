// circle, ellipse, rect, line, // <input type="color" value="#ff0000">
var app = {
    orientation: window.matchMedia("(orientation:landscape)").matches, // true - landscape, false - portrait

    figures: [],
    drawedFigureSettings: null,
    drawedFigure: null,

    activeLever: null,
    figureOnDrag: null,

    command: 'arrow',
    figLastID: 4,

    selectionFrame: null,
    selectionLevers: null,
    mouseButtonPressed: false,
    figureSelectedID: null,
    figureSelected: null
};

function id(id) {
    return document.getElementById(id);
}

app.render_all = function () {
    var type;

    for (var i = 0; i < app.figures.length; i++) {
        type = app.figures[i].type;
        app.render[type](app.figures[i]);
    }
};

app.setCommand = function (el, command) {
    var menu_el = document.getElementById('menu').childNodes;

    if (command === "arrow") {
        document.body.classList.value = "";
    } else if (command === "rectangle") {
        document.body.classList.value = "cursor-pencil-rectangle";
    } else if (command === "ellipse") {
        document.body.classList.value = "cursor-pencil";
    }

    for (var i = 0, max = menu_el.length; i < max; i++) {
        menu_el[i].classList.remove('selected');
    }
    el.classList.add('selected');
    app.command = command;
};

window.onload = function () {
    init();
    statusBarInit('statusBar');
    app.render = init_render();
    eventsInit();
    app.render_all();

};

function eventsInit() {
    window.addEventListener('resize', function () {
        app.orientation = window.matchMedia("(orientation:landscape)").matches;
    });

    // mouse MOOVE
    app.render.svg.addEventListener('mousemove', function (e) {
        var x = (app.orientation) ? e.clientX - 50 : e.clientX,
            y = (app.orientation) ? e.clientY : e.clientY - 50,
            startX,
            startY;

        app.statusBar.showMousePos(x, y);

        // figure re-change by levers
        if (app.mouseButtonPressed && app.activeLever && app.command === "arrow") {
            app.figureResizingByLevers(x, y);
//            app.SelectedFigureAction(app.figureSelectedID);
        }

        //figure mooving
        if (app.mouseButtonPressed && app.command === "arrow" && app.figureOnDrag) {
            app.figureMooving(x, y);
        }

        // rectangle drawing
        if (app.mouseButtonPressed && app.command === "rectangle") {

            if (!app.drawedFigure) {
                app.render[app.command](app.drawedFigureSettings);
                app.drawedFigure = document.getElementById(app.drawedFigureSettings.id);
            } else {
                startX = app.drawedFigureSettings.startX;
                startY = app.drawedFigureSettings.startY;

                if ((x - startX) > 0) {
                    app.drawedFigure.setAttribute('x', startX);
                    app.drawedFigure.setAttribute('width', x - (+app.drawedFigure.getAttribute('x')));
                } else {
                    app.drawedFigure.setAttribute('x', x);
                    app.drawedFigure.setAttribute('width', startX - x);
                }

                if ((y - startY) > 0) {
                    app.drawedFigure.setAttribute('y', startY);
                    app.drawedFigure.setAttribute('height', y - (+app.drawedFigure.getAttribute('y')));
                } else {
                    app.drawedFigure.setAttribute('y', y);
                    app.drawedFigure.setAttribute('height', startY - y);
                }
            }
        }

        // ellipse drawing
        if (app.mouseButtonPressed && app.command === "ellipse") {
            if (!app.drawedFigure) {
                app.render[app.command](app.drawedFigureSettings);
                app.drawedFigure = document.getElementById(app.drawedFigureSettings.id);
            } else {
                startX = app.drawedFigureSettings.startX;
                startY = app.drawedFigureSettings.startY;

                if ((x - startX) > 0) {
                    app.drawedFigure.setAttribute('cx', ((x - startX) / 2) + startX);
                    app.drawedFigure.setAttribute('rx', x - (+app.drawedFigure.getAttribute('cx')));
                } else {
                    app.drawedFigure.setAttribute('cx', ((startX - x) / 2) + x);
                    app.drawedFigure.setAttribute('rx', startX - (+app.drawedFigure.getAttribute('cx')));
                }

                if ((y - startY) > 0) {
                    app.drawedFigure.setAttribute('cy', ((y - startY) / 2) + startY);
                    app.drawedFigure.setAttribute('ry', y - (+app.drawedFigure.getAttribute('cy')));
                } else {
                    app.drawedFigure.setAttribute('cy', ((startY - y) / 2) + y);
                    app.drawedFigure.setAttribute('ry', startY - (+app.drawedFigure.getAttribute('cy')));
                }
            }
        }
    });

    // mouse OVER
    app.render.svg.addEventListener('mouseover', function (e) {
        var info;

        for (var i = 0; i < app.figures.length; i++) {
            if (e.target.id === app.figures[i].id) {
                info = app.figures[i].info || app.figures[i].type;
                app.statusBar.showMouseEnterFigure(info);
                return;
            }
        }
        app.statusBar.showMouseEnterFigure('');
    });

    // mouse CLICK
    app.render.svg.addEventListener('click', function (e) {
        if (app.command === "arrow") {
            if (e.target.id === 'selectionFrame' || e.target.classList.value === 'levers') {
                return;
            }
            if (!(e.target === app.render.svg)) {
                app.SelectedFigureAction(e.target.id);
                app.figureSelectedID = e.target.id;
                app.figureSelected = id(e.target.id);
                app.statusBar.showSelectFigure(e.target.tagName, e.target.id);
            } else {
                app.deleteSelectionFrame();
            }
        }
    });

    // mouse DOWN
    app.render.svg.addEventListener('mousedown', function (e) {
        var x = (app.orientation) ? e.clientX - 50 : e.clientX,
            y = (app.orientation) ? e.clientY : e.clientY - 50;

        app.mouseButtonPressed = true;

        if (e.target.id === "selectionFrame") {
            if (app.figureSelected.tagName === "rect") {
                app.figureOnDrag = {
                    x: x - app.figureSelected.getAttribute('x'),
                    y: y - app.figureSelected.getAttribute('y')
                };
            }
            if (app.figureSelected.tagName === "ellipse") {
                app.figureOnDrag = {
                    x: x - app.figureSelected.getAttribute('cx'),
                    y: y - app.figureSelected.getAttribute('cy')
                };
            }
            
        }

        if (e.target.classList.value === 'levers') {
            app.activeLever = e.target.id;
        }

        if (app.command === "rectangle" || app.command === "ellipse") {
            app.drawedFigureSettings = {
                startX: (app.orientation) ? e.clientX - 50 : e.clientX,
                startY: (app.orientation) ? e.clientY : e.clientY - 50,
                id: 'f_' + ++app.figLastID
            };
        }
    });

    // mouse UP
    app.render.svg.addEventListener('mouseup', function (e) {
        app.mouseButtonPressed = false;

        if (app.figureOnDrag) {
            app.figures.update(app.figureSelectedID);
            app.figureOnDrag = null;
        }

        if (app.command === "rectangle" || app.command === "ellipse") {
            app.figures.add(app.drawedFigure);
            app.drawedFigureSettings = null;
            app.drawedFigure = null;
        }

        if (app.activeLever) {
            app.figures.update(app.figureSelectedID);
            app.activeLever = null;
        }
    });


    window.addEventListener('keydown', function (e) {
        var keyCode = e.keyCode;

        if (keyCode === 46) {
            app.deleteSelectedFigure();
        }
    });

}

function init() {
    document.body.innerHTML = '<div id="menu"></div>'
        + '<svg id="svg"></svg>'
        + '<table id="statusBar">'
        + '<tr>'
        + '<td class="barFrames"></td>'
        + '<td class="barFrames"></td>'
        + '<td class="barFrames"></td>'
        + '<td class="barFrames"></td>'
        + '<td class="barFrames"></td>'
        + '</tr>'
        + '</table>';
    var menu = document.getElementById('menu'),
        el;

    el = document.createElement('img');
    el.src = 'img/buttons/arrow.svg';
    el.className = 'buttons selected';
    el.addEventListener('click', function (e) {
        app.setCommand(e.target, 'arrow');
    });
    menu.appendChild(el);

    el = document.createElement('img');
    el.src = 'img/buttons/rectangle.svg';
    el.className = 'buttons';
    el.addEventListener('click', function (e) {
        app.setCommand(e.target, 'rectangle');
    });
    menu.appendChild(el);

    el = document.createElement('img');
    el.src = 'img/buttons/circle.svg';
    el.className = 'buttons';
    el.addEventListener('click', function (e) {
        app.setCommand(e.target, 'ellipse');
    });
    menu.appendChild(el);


}

function init_render() {
    var render = {
        svg: document.getElementById('svg'),
        xmlns: "http://www.w3.org/2000/svg",

        ellipse: function (obj) {
            var el;

            el = document.createElementNS(this.xmlns, "ellipse");
            el.setAttributeNS(null, "cx", obj.cx || obj.startX);
            el.setAttributeNS(null, "cy", obj.cy || obj.startY);
            el.setAttributeNS(null, "rx", obj.rx || "5");
            el.setAttributeNS(null, "ry", obj.ry || "5");
            el.setAttributeNS(null, "fill", obj.fill || "white");
            el.setAttributeNS(null, "id", obj.id);
            el.setAttributeNS(null, "stroke", obj.stroke || "black");
            el.setAttributeNS(null, "stroke-width", obj.strokeWidth || "1");
            this.svg.appendChild(el);
        },

        rectangle: function (obj) {
            var el;

            el = document.createElementNS(this.xmlns, "rect");
            el.setAttributeNS(null, "x", obj.x || obj.startX);
            el.setAttributeNS(null, "y", obj.y || obj.startY);
            el.setAttributeNS(null, "width", obj.width || "6");
            el.setAttributeNS(null, "height", obj.height || "6");
            el.setAttributeNS(null, "fill", obj.fill || "white");
            el.setAttributeNS(null, "id", obj.id);
            el.setAttributeNS(null, "stroke", obj.stroke || "black");
            el.setAttributeNS(null, "stroke-dasharray", obj.strokeDasharray);
            el.setAttributeNS(null, "stroke-width", obj.strokeWidth || "1");
            if (obj.class) {
                el.setAttributeNS(null, "class", obj.class);
            }
            this.svg.appendChild(el);
        },

        selectionLevers: function (arr) {
            for (var i = 0; i < arr.length; i++) {
                app.render.rectangle(arr[i]);
            }
        }

    };
    return render;
}

app.deleteSelectedFigure = function () {
    var el;

    if (app.command === "arrow" && app.figureSelectedID) {
        el = document.getElementById(app.figureSelectedID);
        el.parentNode.removeChild(el);
        for (var i = 0; i < app.figures.length; i++) {
            if (app.figures[i].id === app.figureSelectedID) {
                app.figures.splice(i, 1);
            }
        }
        app.deleteSelectionFrame();
    }
};

app.deleteSelectionFrame = function () {
    var el;

    app.figureSelectedID = null;
    app.statusBar.showSelectFigure('', '');

    // delete selectedFrame
    if (app.selectionFrame) {
        app.selectionFrame.parentNode.removeChild(app.selectionFrame);
        for (var i = 0; i < app.selectionLevers.length; i++) {
            el = document.getElementById(app.selectionLevers[i].id);
            el.parentNode.removeChild(el);
        }
        app.selectionFrame = null;
    }
};

app.SelectedFigureAction = function (id) {
    if (app.selectionFrame) {
        app.deleteSelectionFrame();
    }
    if (id === 'selectionFrame') {
        return;
    }


    var el = document.getElementById(id),
//        sw = +el.getAttribute('stroke-width'),
        fault = 3,
        obj = {},
        levers = [];

    el = el.getBBox();

    obj.x = el.x;
    obj.y = el.y;
    obj.width = el.width;
    obj.height = el.height;
    obj.fill = "rgba(255, 0, 0 ,0)";
    obj.stroke = '#1c8ffd';
    obj.strokeDasharray = "5,2";
    obj.strokeWidth = '2';
    obj.id = "selectionFrame";

    levers[0] = {x: el.x - fault, y: el.y - fault, id: 'lev_0', class: 'levers'};
    levers[1] = {x: el.x + (el.width / 2) - fault, y: el.y - fault, id: 'lev_1', class: 'levers'};
    levers[2] = {x: el.x + el.width - fault, y: el.y - fault, id: 'lev_2', class: 'levers'};
    levers[3] = {x: el.x + el.width - fault, y: el.y + (el.height / 2) - fault, id: 'lev_3', class: 'levers'};
    levers[4] = {x: el.x + el.width - fault, y: el.y + el.height - fault, id: 'lev_4', class: 'levers'};
    levers[5] = {x: el.x + (el.width / 2) - fault, y: el.y + el.height - fault, id: 'lev_5', class: 'levers'};
    levers[6] = {x: el.x - fault, y: el.y + el.height - fault, id: 'lev_6', class: 'levers'};
    levers[7] = {x: el.x - fault, y: el.y + (el.height / 2) - fault, id: 'lev_7', class: 'levers'};

    app.selectionLevers = levers;

    app.render.rectangle(obj);      //draw selection frame
    app.render.selectionLevers(levers);   //draw selection levers 
    app.selectionFrame = document.getElementById("selectionFrame");

};




function statusBarInit(id) {
    var bar = document.getElementById(id);

    app.statusBar = {
        showMousePos: function (x, y) {
            bar.querySelectorAll('td')[0].innerText = 'x: ' + x + ', y: ' + y;
        },
        showSelectFigure: function (name, id) {
            if (name === '') {
                bar.querySelectorAll('td')[2].innerText = "";
            } else {
                if (name === 'rect') {
                    name = "rectangle";
                }
                bar.querySelectorAll('td')[2].innerText = 'selected: ' + name + ", id: " + id;
            }
        },
        showMouseEnterFigure: function (info) {
            bar.querySelectorAll('td')[3].innerText = info;
        }
    };

}

app.figureResizingByLevers = function (x, y) {
    var figure = document.getElementById(app.figureSelectedID), // svg element
        f = app.figures[app.figures.getPos(app.figureSelectedID)], // index of figure from figures array
        anchorX,
        anchorY;

// Upper Left lever
    if (app.activeLever === 'lev_0') {

        // for rect
        if (figure.tagName === 'rect') {
            anchorX = f.x + f.width;
            anchorY = f.y + f.height;

            if (anchorX - x > 0) {
                figure.setAttribute('x', x);
                figure.setAttribute('width', anchorX - x);
            } else {
                figure.setAttribute('x', anchorX);
                figure.setAttribute('width', x - anchorX);
            }
            if (anchorY - y > 0) {
                figure.setAttribute('y', y);
                figure.setAttribute('height', anchorY - y);
            } else {
                figure.setAttribute('y', anchorY);
                figure.setAttribute('height', y - anchorY);
            }
        }

        // for ellipse
        if (figure.tagName === 'ellipse') {
            anchorX = f.cx + f.rx;
            anchorY = f.cy + f.ry;

            if (anchorX - x > 0) {
                figure.setAttribute('cx', ((anchorX - x) / 2) + x);
                figure.setAttribute('rx', (anchorX - x) / 2);
            } else {
                figure.setAttribute('cx', ((x - anchorX) / 2) + anchorX);
                figure.setAttribute('rx', (x - anchorX) / 2);
            }
            if (anchorY - y > 0) {
                figure.setAttribute('cy', ((anchorY - y) / 2) + y);
                figure.setAttribute('ry', (anchorY - y) / 2);
            } else {
                figure.setAttribute('cy', ((y - anchorY) / 2) + anchorY);
                figure.setAttribute('ry', (y - anchorY) / 2);
            }

        }
    }

// Upper lever
    if (app.activeLever === 'lev_1') {
        if (figure.tagName === 'rect') {
            anchorX = f.x + (f.width / 2);
            anchorY = f.y + f.height;

            if (anchorY - y > 0) {
                figure.setAttribute('y', y);
                figure.setAttribute('height', anchorY - y);
            } else {
                figure.setAttribute('y', anchorY);
                figure.setAttribute('height', y - anchorY);
            }
        }
        if (figure.tagName === 'ellipse') {
            anchorX = f.cx;
            anchorY = f.cy + f.ry;

            if (anchorY - y > 0) {
                figure.setAttribute('cy', ((anchorY - y) / 2) + y);
                figure.setAttribute('ry', (anchorY - y) / 2);
            } else {
                figure.setAttribute('cy', ((y - anchorY) / 2) + anchorY);
                figure.setAttribute('ry', (y - anchorY) / 2);
            }
        }
    }

// Upper Right lever
    if (app.activeLever === 'lev_2') {
        if (figure.tagName === 'rect') {
            anchorX = f.x;
            anchorY = f.y + f.height;

            if (anchorY - y > 0) {
                figure.setAttribute('y', y);
                figure.setAttribute('height', anchorY - y);
            } else {
                figure.setAttribute('y', anchorY);
                figure.setAttribute('height', y - anchorY);
            }
            if (anchorX - x > 0) {
                figure.setAttribute('x', x);
                figure.setAttribute('width', anchorX - x);
            } else {
                figure.setAttribute('x', anchorX);
                figure.setAttribute('width', x - anchorX);
            }
        }
        if (figure.tagName === 'ellipse') {
            anchorX = f.cx - f.rx;
            anchorY = f.cy + f.ry;

            if (anchorY - y > 0) {
                figure.setAttribute('cy', ((anchorY - y) / 2) + y);
                figure.setAttribute('ry', (anchorY - y) / 2);
            } else {
                figure.setAttribute('cy', ((y - anchorY) / 2) + anchorY);
                figure.setAttribute('ry', (y - anchorY) / 2);
            }
            if (anchorX - x > 0) {
                figure.setAttribute('cx', ((anchorX - x) / 2) + x);
                figure.setAttribute('rx', (anchorX - x) / 2);
            } else {
                figure.setAttribute('cx', ((x - anchorX) / 2) + anchorX);
                figure.setAttribute('rx', (x - anchorX) / 2);
            }
        }
    }

// Right lever
    if (app.activeLever === 'lev_3') {

        // for rect
        if (figure.tagName === 'rect') {
            anchorX = f.x;
            anchorY = f.y + (f.height / 2);

            if (anchorX - x > 0) {
                figure.setAttribute('x', x);
                figure.setAttribute('width', anchorX - x);
            } else {
                figure.setAttribute('x', anchorX);
                figure.setAttribute('width', x - anchorX);
            }
        }

        // for ellipse
        if (figure.tagName === 'ellipse') {
            anchorX = f.cx - f.rx;
            anchorY = f.cy;

            if (anchorX - x > 0) {
                figure.setAttribute('cx', ((anchorX - x) / 2) + x);
                figure.setAttribute('rx', (anchorX - x) / 2);
            } else {
                figure.setAttribute('cx', ((x - anchorX) / 2) + anchorX);
                figure.setAttribute('rx', (x - anchorX) / 2);
            }
        }
    }

// Lower Right lever
    if (app.activeLever === 'lev_4') {

        // for rect
        if (figure.tagName === 'rect') {
            anchorX = f.x;
            anchorY = f.y;

            if (anchorX - x > 0) {
                figure.setAttribute('x', x);
                figure.setAttribute('width', anchorX - x);
            } else {
                figure.setAttribute('x', anchorX);
                figure.setAttribute('width', x - anchorX);
            }
            if (anchorY - y > 0) {
                figure.setAttribute('y', y);
                figure.setAttribute('height', anchorY - y);
            } else {
                figure.setAttribute('y', anchorY);
                figure.setAttribute('height', y - anchorY);
            }
        }

        // for ellipse
        if (figure.tagName === 'ellipse') {
            anchorX = f.cx - f.rx;
            anchorY = f.cy - f.ry;

            if (anchorX - x > 0) {
                figure.setAttribute('cx', ((anchorX - x) / 2) + x);
                figure.setAttribute('rx', (anchorX - x) / 2);
            } else {
                figure.setAttribute('cx', ((x - anchorX) / 2) + anchorX);
                figure.setAttribute('rx', (x - anchorX) / 2);
            }
            if (anchorY - y > 0) {
                figure.setAttribute('cy', ((anchorY - y) / 2) + y);
                figure.setAttribute('ry', (anchorY - y) / 2);
            } else {
                figure.setAttribute('cy', ((y - anchorY) / 2) + anchorY);
                figure.setAttribute('ry', (y - anchorY) / 2);
            }
        }
    }

// Lower lever
    if (app.activeLever === 'lev_5') {
        if (figure.tagName === 'rect') {
            anchorX = f.x + (f.width / 2);
            anchorY = f.y;

            if (anchorY - y > 0) {
                figure.setAttribute('y', y);
                figure.setAttribute('height', anchorY - y);
            } else {
                figure.setAttribute('y', anchorY);
                figure.setAttribute('height', y - anchorY);
            }
        }
        if (figure.tagName === 'ellipse') {
            anchorX = f.cx;
            anchorY = f.cy - f.ry;

            if (anchorY - y > 0) {
                figure.setAttribute('cy', ((anchorY - y) / 2) + y);
                figure.setAttribute('ry', (anchorY - y) / 2);
            } else {
                figure.setAttribute('cy', ((y - anchorY) / 2) + anchorY);
                figure.setAttribute('ry', (y - anchorY) / 2);
            }
        }
    }

// Lower Left level
    if (app.activeLever === 'lev_6') {

        // for rect
        if (figure.tagName === 'rect') {
            anchorX = f.x + f.width;
            anchorY = f.y;

            if (anchorX - x > 0) {
                figure.setAttribute('x', x);
                figure.setAttribute('width', anchorX - x);
            } else {
                figure.setAttribute('x', anchorX);
                figure.setAttribute('width', x - anchorX);
            }
            if (anchorY - y > 0) {
                figure.setAttribute('y', y);
                figure.setAttribute('height', anchorY - y);
            } else {
                figure.setAttribute('y', anchorY);
                figure.setAttribute('height', y - anchorY);
            }
        }

        // for ellipse
        if (figure.tagName === 'ellipse') {
            anchorX = f.cx + f.rx;
            anchorY = f.cy - f.ry;

            if (anchorX - x > 0) {
                figure.setAttribute('cx', ((anchorX - x) / 2) + x);
                figure.setAttribute('rx', (anchorX - x) / 2);
            } else {
                figure.setAttribute('cx', ((x - anchorX) / 2) + anchorX);
                figure.setAttribute('rx', (x - anchorX) / 2);
            }
            if (anchorY - y > 0) {
                figure.setAttribute('cy', ((anchorY - y) / 2) + y);
                figure.setAttribute('ry', (anchorY - y) / 2);
            } else {
                figure.setAttribute('cy', ((y - anchorY) / 2) + anchorY);
                figure.setAttribute('ry', (y - anchorY) / 2);
            }
        }
    }

// Left level
    if (app.activeLever === 'lev_7') {

        // for rect
        if (figure.tagName === 'rect') {
            anchorX = f.x + f.width;
            anchorY = f.y + (f.height / 2);

            if (anchorX - x > 0) {
                figure.setAttribute('x', x);
                figure.setAttribute('width', anchorX - x);
            } else {
                figure.setAttribute('x', anchorX);
                figure.setAttribute('width', x - anchorX);
            }
        }

        // for ellipse
        if (figure.tagName === 'ellipse') {
            anchorX = f.cx + f.rx;
            anchorY = f.cy;

            if (anchorX - x > 0) {
                figure.setAttribute('cx', ((anchorX - x) / 2) + x);
                figure.setAttribute('rx', (anchorX - x) / 2);
            } else {
                figure.setAttribute('cx', ((x - anchorX) / 2) + anchorX);
                figure.setAttribute('rx', (x - anchorX) / 2);
            }
        }
    }
};

app.figureMooving = function (x, y) {
    var figure = document.getElementById(app.figureSelectedID);

    if (figure.tagName === "rect") {
        figure.setAttribute('x', x - app.figureOnDrag.x);
        figure.setAttribute('y', y - app.figureOnDrag.y);
    }

    if (figure.tagName === "ellipse") {
        figure.setAttribute('cx', x - app.figureOnDrag.x);
        figure.setAttribute('cy', y - app.figureOnDrag.y);
    }
};




app.figures = [
    {type: 'ellipse', cx: 300, cy: 350, rx: 25, ry: 25, id: 'fig_1', fill: 'yellow', stroke: 'black', strokeWidth: '5', 'info': "голова"},
    {type: 'rectangle', x: 100, y: 150, width: 40, height: 100, id: 'fig_2', fill: '#ffffff', stroke: 'black', strokeWidth: '10', info: "тело"},
    {type: 'ellipse', cx: 500, cy: 150, rx: 150, ry: 50, id: 'fig_3', fill: 'orange', stroke: 'black', strokeWidth: '5'}
];

app.figures.add = function (svg) {
    var obj = {};

    switch (svg.tagName) {
        case 'rect':
            obj.type = 'rectangle';
            obj.x = +svg.getAttribute('x');
            obj.y = +svg.getAttribute('y');
            obj.width = +svg.getAttribute('width');
            obj.height = +svg.getAttribute('height');
            break;

        case 'ellipse':
            obj.type = svg.tagName;
            obj.cx = +svg.getAttribute('cx');
            obj.cy = +svg.getAttribute('cy');
            obj.rx = +svg.getAttribute('rx');
            obj.ry = +svg.getAttribute('ry');
            break;
    }

    obj.id = svg.getAttribute('id');
    obj.fill = svg.getAttribute('fill');
    obj.stroke = svg.getAttribute('stroke');
    obj.strokeWidth = svg.getAttribute('stroke-width');

    app.figures.push(obj);
};

app.figures.getPos = function (id) {
    for (var i = 0; i < app.figures.length; i++) {
        if (app.figures[i].id === id) {
            return i;
        }
    }
};

app.figures.update = function (id) {
    var svg = document.getElementById(id),
        obj = app.figures[app.figures.getPos(id)];

    switch (svg.tagName) {
        case 'rect':
            obj.x = +svg.getAttribute('x');
            obj.y = +svg.getAttribute('y');
            obj.width = +svg.getAttribute('width');
            obj.height = +svg.getAttribute('height');
            break;

        case 'ellipse':
            obj.cx = +svg.getAttribute('cx');
            obj.cy = +svg.getAttribute('cy');
            obj.rx = +svg.getAttribute('rx');
            obj.ry = +svg.getAttribute('ry');
            break;
    }

    obj.fill = svg.getAttribute('fill');
    obj.stroke = svg.getAttribute('stroke');
    obj.strokeWidth = svg.getAttribute('stroke-width');

};



//
//function Figure(conf) {
//    this.x = conf.x;
//    this.y = conf.y;
//    this.stroke = conf.stroke;
//    this.id = 1;
//}
//
//Figure.prototype.show = function (){};
//
//
////    this.fill = conf.fill;
//
//
//function Line(params){
//    this.prototype = new Parent(params);
//    this.x1 = params.x1;
//    this.y1 = params.y1;
//};
//Line.prototype.show = function() {
//  // render   
//};


//
//function Rectangle(){}
//function Circle(){}
//function Ellipse(){}
//
////Rectangle();
////Rectangle.prototype.render();
////Rectangle.prototype.remove();
//
////Circle();
////Circle.prototype.render();
////Circle.prototype.remove();
////
////Ellipse();
////Ellipse.prototype.render();
////Ellipse.prototype.remove();
//
//var rec = new Rectangle({x: 100, y: 100});
//
//var f = [];
//f.push(new Rectangle({x: 100, y: 100, w: 100, h: 100}));
//f.push(new Circle({x: 100, y: 100, r: 100}));
